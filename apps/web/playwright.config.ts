import { defineConfig, devices } from '@playwright/test';

const URL = 'http://localhost:3000';
export default defineConfig({
  testDir: 'tests',
  fullyParallel: true,
  projects: [
    {
      name: 'Chromium',
      use: { ...devices['Desktop Chrome'] },
    },
    {
      name: 'firefox',
      use: { ...devices['Desktop Firefox'] },
    },
  ],
  use: {
    baseURL: URL,
  },

  webServer: {
    command: 'pnpm run build && npm run start',
    url: URL,
    reuseExistingServer: !process.env.CI,
  },
});
