import { Paragraph } from '@fsiot/ui';
import { ComponentProps } from 'react';

type HomeIconProps = ComponentProps<typeof Paragraph>;
declare const HomeIcon: ({ style, ...props }: HomeIconProps) => JSX.Element;

type HomeScreenProps = ComponentProps<typeof Paragraph>;
declare const HomeScreen: (props: HomeScreenProps) => JSX.Element;

export { HomeIcon, HomeScreen };
