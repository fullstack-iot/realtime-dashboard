import { TextProps } from 'react-native';

declare const Paragraph: ({ children, style, ...props }: TextProps) => JSX.Element;

declare const Strong: ({ children, style, ...props }: TextProps) => JSX.Element;

export { Paragraph, Strong };
