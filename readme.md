# Realtime Dashboard

![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/realtime-dashboard?branch=00-scaffolding&logo=gitlab&style=for-the-badge)
![](https://img.shields.io/badge/node-18.17.0-gray?style=for-the-badge&logo=node.js)
![](https://img.shields.io/badge/typescript-5.0.2-gray?style=for-the-badge&logo=typescript)

This is monorepo that targets both mobile and web platforms. It is a realtime dashboard that displays data from a MQTT broker. The project is built using React Native and React. It is organized by feature. Each feature is a package that can be shared between the web and mobile platforms.
